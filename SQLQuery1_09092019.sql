﻿

create database sample

use sample

create table emp (empid int,ename varchar(10),esal float)

select * from emp order by esal desc 

select * from emp where esal not in(20000,800000)

insert into emp (empid,esal)values(100,10000),(2,'pavan',20000),(3,'anu',30000),(4,'meghana',56000)

select @@version


order by clause  asc,desc
-----------------------
select * from emp order by esal desc 


null handling functions
------------------------------

1.isnull(par1,par2)---it gives the first non null value
2.nullif(par1,par2) ---it compares par1 with par2,if is same it give null otherwise it gives first value
3.coalesce(par1,par2,par3...parn)

select empid,isnull(ename,'Na') from emp


select nullif(null,null)

select coalesce(null,null,20)
------------------------------------------------
constraints---

1.primary key--it will allows only unique values
                it wont allow null values

2.foreign key---it is used to make arelationship between two tables
3.unique key--it allows only unique values
            --it will allows only one null value

4.null ---it allows null values
5.not null-- it wont allow null values
6.check---restrict the values entering into column based on condition
7.default---it will take default values

select * into emp_temp from emp where 1=2

sp_help 'emp_temp'
drop table emp
create table emp(empid int primary key)

alter table emp add esal int check(esal>4000 )

alter table emp add gender varchar(10) default 'male'
select * from emp


insert into emp (empid,ename,esal)values(103,'bhavya',4001)

